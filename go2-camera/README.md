## 依赖

```
sudo apt-get install libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libgstreamer-plugins-bad1.0-dev gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-libav gstreamer1.0-tools gstreamer1.0-x gstreamer1.0-alsa gstreamer1.0-gl gstreamer1.0-gtk3 gstreamer1.0-qt5 gstreamer1.0-pulseaudio
```

## opencv

接入 Go2 的相机，需要在编译 OpenCV 时勾选 WITH_GSTREAMER 选项。

```
cd opencv
mkdir build
cd build
cmake -D WITH_GTK=ON -D WITH_GSTREAMER=ON -D WITH_GSTREAMER_0_10=OFF -D CMAKE_BUILD_TYPE=Release ..
make -j12
sudo make install
```
