#ifndef _HEADER_H_
#define _HEADER_H_

#include <stdint.h>

#define SLICE_LEN 1400

/**
 * 分片头信息, 32字节
 */
typedef struct aiit_head {
	char sync_code[4];  /* 4字节，固定为 AIIT */
	uint32_t frame_num; /* 4字节，码流帧编号 */
	uint32_t frame_len; /* 4字节，码流帧大小 */
	                    /* 4字节，考虑到内存对齐，这里会插入4字节以保证后面的 pts 在8字节对齐位置 */
	uint64_t frame_pts; /* 8字节，码流帧时间戳 */
	uint16_t slice_cnt; /* 2字节，码流分片传输, 分片数量 */
	uint16_t slice_num; /* 2字节，码流分片传输, 分片序号 */
	uint32_t slice_len; /* 4字节，码流分片传输, 分片大小 */
} aiit_head_t;

#endif
