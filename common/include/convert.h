#ifndef _CONVERT_H_
#define _CONVERT_H_

#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
#include <libavutil/opt.h>
#include <libswscale/swscale.h>

int convert(AVFrame* inframe, AVFrame* outframe);

#endif
