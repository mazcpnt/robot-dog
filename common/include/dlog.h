#ifndef _DLOG_H_
#define _DLOG_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>

#define DERR 0      /* error */
#define DWAN 1      /* warning */
#define DINF 2      /* infomation */
#define DBUG 3      /* debug */
#define DIGN 4      /* ignore */
#define DFAT 0x55AA /* default */
#define DCOM DINF   /* common */

// clang-format off
#define DLVL_STR(lvl)            \
	(DERR == lvl) ? "DERR" : \
	(DWAN == lvl) ? "DWAN" : \
	(DINF == lvl) ? "DINF" : \
	(DBUG == lvl) ? "DBUG" : \
	(DIGN == lvl) ? "DIGN" : \
	"UNKNOW"
// clang-format on

#define dmsg(lvl, fmt, arg...)      \
	if (lvl <= dlvl) {          \
		printf(fmt, ##arg); \
	}

#define dbug(lvl, fmt, arg...)           \
	if (lvl <= dlvl) {               \
		printf(fmt "\n", ##arg); \
	}

#define dlog(lvl, fmt, arg...)                                                \
	if (lvl <= dlvl) {                                                    \
		printf("[%04d][%s]" fmt "\n", __LINE__, __FUNCTION__, ##arg); \
	}

#ifdef __cplusplus
}
#endif

#endif /* _DLOG_H_ */
