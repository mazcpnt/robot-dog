#include "convert.h"
#include "dlog.h"

static int dlvl = DINF;

int convert(AVFrame* inframe, AVFrame* outframe)
{
	int ret = 0;
	struct SwsContext* sws_ctx = NULL;

	sws_ctx = sws_getContext(
	    inframe->width,
	    inframe->height,
	    inframe->format,
	    outframe->width,
	    outframe->height,
	    outframe->format,
	    SWS_BILINEAR,
	    NULL, NULL, NULL);
	if (NULL == sws_ctx) {
		dlog(DERR, "err: sws_getContext()");
		return -1;
	}

	ret = sws_scale(sws_ctx,
	    (const uint8_t* const*)inframe->data,
	    inframe->linesize,
	    0,
	    inframe->height,
	    outframe->data,
	    outframe->linesize);
	if (ret != inframe->height) {
		dlog(DERR, "err: sws_scale()");
		return -1;
	}

	sws_freeContext(sws_ctx);

	return 0;
}
