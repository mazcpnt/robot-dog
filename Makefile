
all: client server

client:

server:
	@make -C server --no-print-directory

clean:
	@make clean -C client --no-print-directory
	@make clean -C server --no-print-directory

.PHONY: all client server clean
