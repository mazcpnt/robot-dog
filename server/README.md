# 查看 YUV 图片

```
main.c 中将 FRAME_DUMP 宏设置为 1
#define FRAME_DUMP 1

ffplay -f rawvideo -pixel_format yuyv422 -video_size 1280x720 capture-0-49.yuyv
ffplay -f rawvideo -pixel_format yuv422p -video_size 1280x720 capture-0-49.yuv422p
ffplay -f rawvideo -pixel_format yuv420p10le -video_size 1280x720 capture-0-49.yuv420p10
```