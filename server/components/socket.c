#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include "components/socket.h"
#include "dlog.h"
#include "header.h"

static int dlvl = DINF;

static void* _socket_server_pthread(void* arg)
{
	int fd = 0;
	int new_fd = 0;
	int opt = 0;
	int ret = 0;
	int rcnt = 0;
	int flags = 0;
	fd_set readfds;
	struct timeval tv;
	struct sockaddr_in address;
	int addrlen = sizeof(address);
	socket_server_t* skt = (socket_server_t*)arg;

	fd = socket(AF_INET, SOCK_STREAM, 0);
	if (fd == 0) {
		dlog(DERR, "err: socket()");
		return NULL;
	}

	opt = 1;
	ret = setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
	if (ret) {
		dlog(DERR, "err: setsockopt(SO_REUSEADDR)");
		return NULL;
	}

	opt = 1;
	ret = setsockopt(fd, SOL_SOCKET, SO_REUSEPORT, &opt, sizeof(opt));
	if (ret) {
		dlog(DERR, "err: setsockopt(SO_REUSEPORT)");
		return NULL;
	}

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(8080);
	if (bind(fd, (struct sockaddr*)&address, sizeof(address)) < 0) {
		dlog(DERR, "err: bind(8080)");
		return NULL;
	}

	if (listen(fd, 3) < 0) {
		dlog(DERR, "err: listen()");
		return NULL;
	}

	flags = fcntl(fd, F_GETFL, 0);
	if (flags == -1) {
		dlog(DERR, "err: fcntl(F_GETFL)");
		return NULL;
	}

	flags |= O_NONBLOCK;
	if (fcntl(fd, F_SETFL, flags) == -1) {
		dlog(DERR, "err: fcntl(F_SETFL)");
		return NULL;
	}

	while (!skt->ctx.pthread_exit) {
		if (skt->ctx.pthread_bypass) {
			usleep(5000);
			continue;
		}

		FD_ZERO(&readfds);
		FD_SET(fd, &readfds);
		tv.tv_sec = 2;
		tv.tv_usec = 0;

		ret = select(fd + 1, &readfds, NULL, NULL, &tv);
		if (ret == -1) {
			dlog(DERR, "err: select()");
			break;
		} else if (ret == 0) {
			continue;
		} else if (FD_ISSET(fd, &readfds)) {
			dbug(DBUG, "[server] wait the client ...");
			if ((new_fd = accept(fd, (struct sockaddr*)&address, (socklen_t*)&addrlen)) < 0) {
				dlog(DERR, "err: accept()");
				continue;
			}
			dbug(DBUG, "[server] accept client");

			skt->ctx.server_fd = fd;
			skt->ctx.client_fd = new_fd;
			skt->ctx.bypass = 0;
		}
	}

	return NULL;
}

static int _socket_server_pthread_create(socket_server_t* skt)
{
	int ret = 0;

	skt->ctx.pthread_bypass = true;
	skt->ctx.pthread_exit = false;
	ret = pthread_create(&skt->ctx.pid, NULL, _socket_server_pthread, skt);
	if (ret) {
		dlog(DERR, "err: pthread_create(pthread, NULL, _socket_server_pthread, &socket_server_t), ret = %d", ret);
		return -1;
	}
	dbug(DBUG, "success: pthread_create(pthread, NULL, _socket_server_pthread, &socket_server_t)");

	return 0;
}

static int _socket_server_pthread_destory(socket_server_t* skt)
{
	int ret = 0;

	skt->ctx.pthread_exit = true;
	ret = pthread_join(skt->ctx.pid, NULL);
	if (ret) {
		dlog(DERR, "err: pthread_join(enc_pthread, NULL), ret = %d", ret);
		return -1;
	}
	dbug(DBUG, "success: pthread_join(enc_pthread, NULL)");

	return 0;
}

int socket_server_init(socket_server_t* skt)
{

	if (NULL == skt) {
		dlog(DERR, "inval: skt");
		return -1;
	}

	skt->ctx.bypass = 1;
	_socket_server_pthread_create(skt);

	return 0;
}

void socket_server_deinit(socket_server_t* skt)
{
	if (NULL == skt) {
		dlog(DERR, "inval: skt");
		return;
	}

	/* 需要先关闭套接字，才能让线程中的 accept 退出阻塞状态 */
	close(skt->ctx.server_fd);
	close(skt->ctx.client_fd);
	_socket_server_pthread_destory(skt);
}

int socket_server_start(socket_server_t* skt)
{
	if (NULL == skt) {
		dlog(DERR, "inval: skt");
		return -1;
	}

	skt->ctx.pthread_bypass = false;

	return 0;
}

void socket_server_stop(socket_server_t* skt)
{
	if (NULL == skt) {
		dlog(DERR, "inval: skt");
		return;
	}

	skt->ctx.pthread_bypass = true;
}

int socket_server_send(socket_server_t* skt, uint8_t* buffer, uint32_t size)
{
	if (NULL == skt) {
		dlog(DERR, "inval: skt");
		return -1;
	}

	return send(skt->ctx.client_fd, buffer, size, 0);
}

int socket_server_packet_send(socket_server_t* skt, uint8_t* addr, uint32_t len, uint64_t pts)
{
	int i = 0;
	int scnt = 0;              // send count
	int rcnt = 0;              // recv count
	int slice_cnt = 0;         // 一共分成多少个包
	int last_slice_len = 0;    // 最后一个包长度是多少
	int slice_len = 0;         // 每个分包的长度
	int head_len = 0;          // 每个分包的头长度
	uint8_t* slice_buf = NULL; // 每个分包的数据
	aiit_head_t head = { 0 };
	aiit_head_t recv_head = { 0 };

	if (NULL == skt) {
		dlog(DERR, "inval: skt");
		return -1;
	}

	if (skt->ctx.bypass) {
		dbug(DBUG, "server socket send bypass");
		return 0;
	}

	head_len = sizeof(aiit_head_t);
	slice_len = head_len + SLICE_LEN;
	slice_buf = malloc(slice_len);

	/* 对码流文件进行手动拆包 */
	slice_cnt = len / SLICE_LEN + 1;
	last_slice_len = len - (slice_cnt - 1) * SLICE_LEN + head_len;

	head.sync_code[0] = 'A';
	head.sync_code[1] = 'I';
	head.sync_code[2] = 'I';
	head.sync_code[3] = 'T';
	head.frame_num = 0;
	head.frame_len = len;
	head.frame_pts = pts;
	head.slice_cnt = slice_cnt;
	for (i = 0; i < slice_cnt; i++) {
		/* 给 client 发送码流数据包 */
		head.slice_num = i;
		head.slice_len = (i != (slice_cnt - 1)) ? slice_len : last_slice_len;
		memcpy(slice_buf, &head, head_len);
		memcpy(slice_buf + head_len, addr + SLICE_LEN * i, head.slice_len - head_len);
		scnt = send(skt->ctx.client_fd, slice_buf, head.slice_len, 0);
		if (scnt != head.slice_len) {
			dlog(DERR, "err: send(%d), scnt = %d", i, scnt);
		}

		/* 接收 client 返回的接收状态 */
		rcnt = recv(skt->ctx.client_fd, &recv_head, head_len, 0);
		if (rcnt != head_len) {
			dlog(DERR, "err: recv(%d), rcnt = %d", i, rcnt);
		}

		if (recv_head.sync_code[0] == 'E'
		    && recv_head.sync_code[1] == 'X'
		    && recv_head.sync_code[2] == 'I'
		    && recv_head.sync_code[3] == 'T') {
			dlog(DBUG, "client disconnect");
			skt->ctx.bypass = 1;
			return 0;
		}

		dbug(DIGN, "%c%c%c%c %-6d %-6d %-6d %-6d",
		    recv_head.sync_code[0],
		    recv_head.sync_code[1],
		    recv_head.sync_code[2],
		    recv_head.sync_code[3],
		    recv_head.frame_len,
		    recv_head.slice_cnt,
		    recv_head.slice_num,
		    recv_head.slice_len);
	}

	return 0;
}

/**
 * @brief
 * @param skt
 * @param buffer 接收缓存区
 * @param size 接收缓存区大小
 * @return 实际接收的数据大小
 */
int socket_server_recv(socket_server_t* skt, uint8_t* buffer, uint32_t size)
{
	if (NULL == skt) {
		dlog(DERR, "inval: skt");
		return -1;
	}

	return recv(skt->ctx.client_fd, buffer, size, 0);
}
