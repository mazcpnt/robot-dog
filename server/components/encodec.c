#include "components/encodec.h"
#include "components/socket.h"

#include "dlog.h"

#include <unistd.h>

static int dlvl = DINF;
extern socket_server_t skt;

/**
 * @brief 摄像头取流线程
 * @param arg 摄像头对象
 */
static void* _encodec_pthread(void* arg)
{
	int ret = 0;
#if STREAM_DUMP
	FILE* file = NULL;
	const char* filename = "out.h266";
#endif
	AVPacket* packet = NULL;
	AVCodecContext* codec_ctx = NULL;
	encodec_t* enc = (encodec_t*)arg;

	packet = av_packet_alloc();
	if (!packet) {
		return NULL;
	}

	codec_ctx = enc->ctx.codec_ctx;

#if STREAM_DUMP
	file = fopen(filename, "wb");
	if (!file) {
		dlog(DERR, "err: fopen(%s)", filename);
		exit(1);
	}
#endif

	while (!enc->ctx.pthread_exit) {
		if (enc->ctx.pthread_bypass) {
			usleep(5000);
			continue;
		}

		pthread_mutex_lock(&enc->ctx.mutex);
		ret = avcodec_receive_packet(codec_ctx, packet);
		pthread_mutex_unlock(&enc->ctx.mutex);
		if (ret == AVERROR(EAGAIN)) {
			usleep(2000);
			continue;
		} else if (ret == AVERROR_EOF) {
			usleep(2000);
			continue;
		} else if (ret < 0) {
			dlog(DERR, "err: Error during encoding");
			return NULL;
		}

		dbug(DIGN, "avcodec_receive_packet, pts = %ld (size=%5d)", packet->pts, packet->size);
		socket_server_packet_send(&skt, packet->data, packet->size, packet->pts);
#if STREAM_DUMP
		fwrite(packet->data, 1, packet->size, file);
#endif
		av_packet_unref(packet);
	}

	av_packet_free(&packet);

#if STREAM_DUMP
	fclose(file);
#endif
}

static int _encodec_pthread_create(encodec_t* enc)
{
	int ret = 0;

	enc->ctx.pthread_bypass = true;
	enc->ctx.pthread_exit = false;
	ret = pthread_create(&enc->ctx.pid, NULL, _encodec_pthread, enc);
	if (ret) {
		dlog(DERR, "err: pthread_create(pthread, NULL, _encodec_pthread, &encodec_t), ret = %d", ret);
		return -1;
	}
	dbug(DBUG, "success: pthread_create(pthread, NULL, _encodec_pthread, &encodec_t)");

	return 0;
}

static int _encodec_pthread_destory(encodec_t* enc)
{
	int ret = 0;

	enc->ctx.pthread_exit = true;
	ret = pthread_join(enc->ctx.pid, NULL);
	if (ret) {
		dlog(DERR, "err: pthread_join(enc_pthread, NULL), ret = %d", ret);
		return -1;
	}
	dbug(DBUG, "success: pthread_join(enc_pthread, NULL)");

	return 0;
}

int encodec_init(encodec_t* enc)
{
	int ret = 0;
	const AVCodec* codec = NULL;
	AVCodecContext* codec_ctx = NULL;

	if (NULL == enc) {
		dlog(DERR, "inval: enc");
	}

	codec = avcodec_find_encoder_by_name("libvvenc");
	if (!codec) {
		dlog(DERR, "err: avcodec_find_encoder_by_name(libvvenc)");
		return -1;
	}

	/* 获取编码器上下文，编码参数需要在上下文中进行配置 */
	codec_ctx = avcodec_alloc_context3(codec);
	if (!codec_ctx) {
		dlog(DERR, "avcodec_alloc_context3()\n");
		return -1;
	}

	/* 配置编码参数 */
	codec_ctx->bit_rate = 800000;
	codec_ctx->width = enc->cfg.var.width;
	codec_ctx->height = enc->cfg.var.height;
	codec_ctx->time_base = (AVRational) { 1, 10 };
	codec_ctx->framerate = (AVRational) { 10, 1 };
	codec_ctx->gop_size = 1;
	codec_ctx->max_b_frames = 1;
	codec_ctx->thread_count = 16;
	codec_ctx->pix_fmt = AV_PIX_FMT_YUV420P10LE;
	av_opt_set(codec_ctx->priv_data, "preset", "faster", 0);

	/* 打开编码器 */
	ret = avcodec_open2(codec_ctx, codec, NULL);
	if (ret < 0) {
		dlog(DERR, "avcodec_open2(%s)\n", av_err2str(ret));
		return -1;
	}

	/* 将编码器上下文保存在 enc 对象中 */
	enc->ctx.codec_ctx = codec_ctx;

	/* 创建互斥锁 */
	ret = pthread_mutex_init(&enc->ctx.mutex, 0);
	if (ret) {
		dlog(DERR, "err: pthread_mutex_init()");
	}

	/* 创建取流线程 */
	_encodec_pthread_create(enc);

	return 0;
}

void encodec_deinit(encodec_t* enc)
{
	if (NULL == enc) {
		dlog(DERR, "inval: enc");
		return;
	}

	if (NULL == enc->ctx.codec_ctx) {
		dlog(DERR, "inval: codec_ctx");
		return;
	}

	_encodec_pthread_destory(enc);
	pthread_mutex_destroy(&enc->ctx.mutex);
	avcodec_free_context(&enc->ctx.codec_ctx);
}

int encodec_start(encodec_t* enc)
{
	if (NULL == enc) {
		dlog(DERR, "inval: enc");
		return -1;
	}

	if (NULL == enc->ctx.codec_ctx) {
		dlog(DERR, "inval: codec_ctx");
		return -1;
	}

	enc->ctx.pthread_bypass = false;

	return 0;
}

void encodec_stop(encodec_t* enc)
{
	if (NULL == enc) {
		dlog(DERR, "inval: enc");
		return;
	}

	if (NULL == enc->ctx.codec_ctx) {
		dlog(DERR, "inval: codec_ctx");
		return;
	}

	enc->ctx.pthread_bypass = true;
}

int encodec_encode(encodec_t* enc, AVFrame* frame)
{
	int ret = 0;
	static int ecnt = 0;

	if (NULL == enc) {
		dlog(DERR, "inval: enc");
		return -1;
	}

	if (NULL == enc->ctx.codec_ctx) {
		dlog(DERR, "inval: codec_ctx");
		return -1;
	}

	pthread_mutex_lock(&enc->ctx.mutex);
	ret = avcodec_send_frame(enc->ctx.codec_ctx, frame);
	pthread_mutex_unlock(&enc->ctx.mutex);

	if (ret < 0) {
		dlog(DERR, "err: avcodec_send_frame(), ret = %d", ret);
		return -1;
	}
	dbug(DIGN, "avcodec_send_frame(%d), pts = %ld", ecnt++, frame->pts);

	return 0;
}