#include <fcntl.h>
#include <linux/videodev2.h>
#include <poll.h>
#include <pthread.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <time.h>
#include <unistd.h>

#include "dlog.h"
#include "drivers/camera.h"

#define FPS_DEBUG 1

static int dlvl = DINF;

struct v4l2_pixfmt {
	int val;
	char str[32];
};

struct v4l2_pixfmt pixfmts[] = {
	{ V4L2_PIX_FMT_YUYV, "V4L2_PIX_FMT_YUYV" },
	{ V4L2_PIX_FMT_UYVY, "V4L2_PIX_FMT_UYVY" },
	{ V4L2_PIX_FMT_YVYU, "V4L2_PIX_FMT_YVYU" },
	{ V4L2_PIX_FMT_VYUY, "V4L2_PIX_FMT_YUYV" },
	{ V4L2_PIX_FMT_NV12, "V4L2_PIX_FMT_NV12" },
	{ V4L2_PIX_FMT_NV21, "V4L2_PIX_FMT_NV21" },
	{ V4L2_PIX_FMT_MJPEG, "V4L2_PIX_FMT_MJPEG" },
};

/**
 * @brief 获取 V4L2 像素格式对应的字符串
 * @param val 像素格式枚举值
 * @retval char* 像素格式字符串
 */
static char* _v4l2_pixfmt_val2str(int val)
{
	int i = 0;

	for (i = 0; i < sizeof(pixfmts) / sizeof(struct v4l2_pixfmt); i++) {
		if (val == pixfmts[i].val) {
			return pixfmts[i].str;
		}
	}

	return NULL;
}

/**
 * @brief 摄像头取流线程
 * @param arg 摄像头对象
 */
static void* _camera_pthread(void* arg)
{
	int fd = 0;
	int ret = 0;
	int chn = 0;
	int num = 0;
	int drop = 0;
	struct pollfd fds[1];
	struct v4l2_buffer vbuf;
	camera_t* cam = (camera_t*)arg;
	frame_t frm;

	dlog(DINF, "enter");

#if FPS_DEBUG
	long boot_pts = 0;
	long curr_pts = 0;
	long last_pts = 0;
	float avg_fps;
	float cur_fps;
#endif

	fd = cam->ctxt.fd;
	fds[0].fd = fd;
	fds[0].events = POLLIN;

	while (!cam->ctxt.pthread_exit) {

		if (cam->ctxt.pthread_bypass) {
			usleep(5000);
			continue;
		}

		ret = poll(fds, 1, -1);

		memset(&vbuf, 0, sizeof(struct v4l2_buffer));
		vbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		vbuf.memory = V4L2_MEMORY_MMAP;
		ret = ioctl(fd, VIDIOC_DQBUF, &vbuf);
		if (ret) {
			dlog(DERR, "err: frame %d, index = %d, ioctl(%d, VIDIOC_DQBUF, &v4l2_buffer), ret = %d", num, vbuf.index, fd, ret);
			usleep(5000);
			continue;
		}

		if (drop++ % 3 != 0) { /* 60fps 丢帧后转化为 20fps */
			ret = ioctl(fd, VIDIOC_QBUF, &vbuf);
			if (ret) {
				dlog(DERR, "err: frame %d, index = %d, ioctl(%d, VIDIOC_QBUF, &v4l2_buffer), ret = %d", num, vbuf.index, fd, ret);
			}
			continue;
		}

		if (cam->ctxt.cb) {
			frm.index = vbuf.index;
			frm.num = num;
			frm.width = cam->ctxt.buf[frm.index].width;
			frm.height = cam->ctxt.buf[frm.index].height;
			frm.bpp = cam->ctxt.buf[frm.index].bpp;
			frm.pitch = cam->ctxt.buf[frm.index].pitch;
			frm.size = vbuf.bytesused;
			frm.addr = cam->ctxt.buf[frm.index].addr;
			frm.pts = vbuf.timestamp.tv_sec * 1000 + vbuf.timestamp.tv_usec / 1000;
			cam->ctxt.cb(chn, &frm);
		}

#if FPS_DEBUG
		/* 统计实时帧率和平均帧率, 单位ms */
		curr_pts = vbuf.timestamp.tv_sec * 1000 + vbuf.timestamp.tv_usec / 1000;
		if (0 == boot_pts && 0 == num) {
			boot_pts = curr_pts;
			dbug(DBUG, "info: chn %d boot pts = %ld", chn, boot_pts);
		}

		cur_fps = 1000.0f / (float)(curr_pts - last_pts);
		avg_fps = num * 1000.0f / (float)(curr_pts - boot_pts);
		last_pts = curr_pts;

		/* num++ 必须放在计算帧率之后, 否则帧率计算不准确 */
		num++;

		/* 调试打印信息, 默认不打印 */
		dbug(DIGN, "ingore: chn %d, idx %d, num %d, timestamp %ld, curr fps %.2f, avg_fps %.2f",
		    chn, frm.index, frm.num, curr_pts, cur_fps, avg_fps);

		if (0 == (num % 25)) {
			dbug(DINF, "info: chn %d num %d fps %.3f", chn, num, avg_fps);
		}
#else
		num++;
#endif

		ret = ioctl(fd, VIDIOC_QBUF, &vbuf);
		if (ret) {
			dlog(DERR, "err: frame %d, index = %d, ioctl(%d, VIDIOC_QBUF, &v4l2_buffer), ret = %d", num, vbuf.index, fd, ret);
			usleep(5000);
			continue;
		}
	}

	dlog(DINF, "exit");
}

/**
 * @brief 创建摄像头取流线程
 * @param cam 摄像头对象
 * @retval int 错误码
 */
static int _camera_pthread_create(camera_t* cam)
{
	int ret = 0;

	cam->ctxt.pthread_bypass = true;
	cam->ctxt.pthread_exit = false;
	ret = pthread_create(&cam->ctxt.pid, NULL, _camera_pthread, cam);
	if (ret) {
		dlog(DERR, "err: pthread_create(pthread, NULL, _camera_pthread, &camera_t), ret = %d", ret);
		return -1;
	}
	dbug(DBUG, "success: pthread_create(pthread, NULL, _camera_pthread, &camera_t)");

	return 0;
}

/**
 * @brief 销毁摄像头取流线程
 * @param cam 摄像头对象
 * @retval int 错误码
 */
static int _camera_pthread_destory(camera_t* cam)
{
	int ret = 0;

	cam->ctxt.pthread_exit = true;
	ret = pthread_join(cam->ctxt.pid, NULL);
	if (ret) {
		dlog(DERR, "err: pthread_join(pthread, NULL), ret = %d", ret);
		return -1;
	}
	dbug(DBUG, "success: pthread_join(pthread, NULL)");

	return 0;
}

/**
 * @brief 初始化 uvc 摄像头
 * @param cam 摄像头对象，需要配置好 cfg 参数
 * @retval int 错误码
 */
int camera_init(camera_t* cam)
{
	int i = 0;
	int ret = 0;
	struct v4l2_capability vcap;
	struct v4l2_fmtdesc vfdesc;
	struct v4l2_format vfmt;
	struct v4l2_buffer vbuf;
	struct v4l2_requestbuffers vreqbuf;

	if (NULL == cam) {
		dlog(DERR, "inval: cam");
		return -1;
	}

	if (NULL == cam->cfg.fix.devname) {
		dlog(DERR, "inval: devname");
		return -1;
	}

	if (0 == cam->cfg.fix.width) {
		dlog(DERR, "inval: width");
		return -1;
	}

	if (0 == cam->cfg.fix.height) {
		dlog(DERR, "inval: height");
		return -1;
	}

	cam->ctxt.fd = open(cam->cfg.fix.devname, O_RDWR);
	if (cam->ctxt.fd < 0) {
		dlog(DERR, "err: open(%s, O_RDWR), ret = %d", cam->cfg.fix.devname, cam->ctxt.fd);
		return -1;
	}
	dbug(DBUG, "success: open(%s, O_RDWR), cam->ctxt.fd = %d", cam->cfg.fix.devname, cam->ctxt.fd);

	ret = ioctl(cam->ctxt.fd, VIDIOC_QUERYCAP, &vcap);
	if (ret) {
		dlog(DERR, "err: ioctl(%d, VIDIOC_QUERYCAP, &v4l2_capability), ret = %d", cam->ctxt.fd, ret);
		goto error_close_device;
	}
	dbug(DBUG, "success: ioctl(%d, VIDIOC_QUERYCAP, &v4l2_capability)", cam->ctxt.fd);

	if (!(vcap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
		dlog(DERR, "err: %s not support V4L2_CAP_VIDEO_CAPTURE", cam->cfg.fix.devname);
		goto error_close_device;
	}
	dbug(DBUG, "success: %s support V4L2_CAP_VIDEO_CAPTURE", cam->cfg.fix.devname);

	if (!(vcap.capabilities & V4L2_CAP_STREAMING)) {
		dlog(DERR, "err: %s not support V4L2_CAP_STREAMING", cam->cfg.fix.devname);
		goto error_close_device;
	}
	dbug(DBUG, "success: %s support V4L2_CAP_STREAMING", cam->cfg.fix.devname);

	memset(&vfdesc, 0, sizeof(vfdesc));
	vfdesc.index = 0;
	vfdesc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	while ((ret = ioctl(cam->ctxt.fd, VIDIOC_ENUM_FMT, &vfdesc)) == 0) {
		dbug(DBUG, "success: index %d, format %s, 0x%08x", vfdesc.index, _v4l2_pixfmt_val2str(vfdesc.pixelformat), vfdesc.pixelformat);
		vfdesc.index++;
	}

	memset(&vfmt, 0, sizeof(struct v4l2_format));
	vfmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	vfmt.fmt.pix.pixelformat = V4L2_PIX_FMT_MJPEG;
	vfmt.fmt.pix.width = cam->cfg.fix.width;
	vfmt.fmt.pix.height = cam->cfg.fix.height;
	vfmt.fmt.pix.field = V4L2_FIELD_ANY;

	/* 如果驱动程序发现无法某些参数(比如分辨率), 它会调整这些参数, 并且返回给应用程序 */
	ret = ioctl(cam->ctxt.fd, VIDIOC_S_FMT, &vfmt);
	if (ret) {
		dlog(DERR, "err: ioctl(%d, VIDIOC_S_FMT, &v4l2_format), ret = %d", cam->ctxt.fd, ret);
		goto error_close_device;
	}
	dbug(DBUG, "success: ioctl(%d, VIDIOC_S_FMT, &v4l2_format)", cam->ctxt.fd);
	dbug(DINF, "success: Format  : %s", _v4l2_pixfmt_val2str(vfdesc.pixelformat));
	dbug(DINF, "success: width   : %d", vfmt.fmt.pix.width);
	dbug(DINF, "success: height  : %d", vfmt.fmt.pix.height);

	/* 设置亮度值 */
	struct v4l2_control control;
	memset(&control, 0, sizeof(control));
	control.id = V4L2_CID_BRIGHTNESS;
	control.value = 40;
	if (ioctl(cam->ctxt.fd, VIDIOC_S_CTRL, &control) == -1) {
		perror("VIDIOC_S_CTRL");
		return EXIT_FAILURE;
	}

	/* 5、VIDIOC_REQBUFS 申请buffer */
	memset(&vreqbuf, 0, sizeof(struct v4l2_requestbuffers));
	vreqbuf.count = CAMERA_BUFFER_CNT;
	vreqbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	vreqbuf.memory = V4L2_MEMORY_MMAP;
	ret = ioctl(cam->ctxt.fd, VIDIOC_REQBUFS, &vreqbuf);
	if (ret) {
		dlog(DERR, "err: ioctl(%d, VIDIOC_REQBUFS, &v4l2_requestbuffers), ret = %d", cam->ctxt.fd, ret);
		goto error_close_device;
	}
	dbug(DBUG, "success: ioctl(%d, VIDIOC_REQBUFS, &v4l2_requestbuffers)", cam->ctxt.fd);

	/* 查询 buffer 的信息，主要是要拿到 buffer 的长度信息 */
	for (i = 0; i < vreqbuf.count; i++) {
		memset(&vbuf, 0, sizeof(struct v4l2_buffer));
		vbuf.index = i;
		vbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		vbuf.memory = V4L2_MEMORY_MMAP;

		ret = ioctl(cam->ctxt.fd, VIDIOC_QUERYBUF, &vbuf);
		if (ret) {
			dlog(DERR, "err: buffer index = %d, ioctl(%d, VIDIOC_QUERYBUF, &v4l2_buffer), ret = %d", vbuf.index, cam->ctxt.fd, ret);
			goto error_unmap_buffers;
		}
		dbug(DBUG, "success: buffer index = %d, ioctl(%d, VIDIOC_QUERYBUF, &v4l2_buffer)", vbuf.index, cam->ctxt.fd);

		cam->ctxt.buf[i].addr = (uint8_t*)mmap(0, vbuf.length, PROT_READ, MAP_SHARED, cam->ctxt.fd, vbuf.m.offset);
		if (cam->ctxt.buf[i].addr == MAP_FAILED) {
			dlog(DERR, "err: buffer index = %d, mmap(0, %d, PROT_READ, MAP_SHARED, %d, %d)", vbuf.index, vbuf.length, cam->ctxt.fd, vbuf.m.offset);
			goto error_unmap_buffers;
		}
		dbug(DBUG, "success: buffer index = %d, mmap(0, %d, PROT_READ, MAP_SHARED, %d, %d)", vbuf.index, vbuf.length, cam->ctxt.fd, vbuf.m.offset);

		cam->ctxt.buf[i].width = vfmt.fmt.pix.width;
		cam->ctxt.buf[i].height = vfmt.fmt.pix.height;
		cam->ctxt.buf[i].bpp = 16;
		cam->ctxt.buf[i].pitch = cam->ctxt.buf[i].width * cam->ctxt.buf[i].bpp / 8;
		cam->ctxt.buf[i].size = cam->ctxt.buf[i].pitch * cam->ctxt.buf[i].height;

		ret = ioctl(cam->ctxt.fd, VIDIOC_QBUF, &vbuf);
		if (ret) {
			dlog(DERR, "err: buffer index = %d, ioctl(%d, VIDIOC_QBUF, &v4l2_buffer), ret = %d", vbuf.index, cam->ctxt.fd, ret);
			i++;
			goto error_unmap_buffers;
		}
		dbug(DBUG, "success: buffer index = %d, ioctl(%d, VIDIOC_QBUF, &v4l2_buffer)", vbuf.index, cam->ctxt.fd);
	}

	ret = _camera_pthread_create(cam);
	if (ret) {
		dlog(DERR, "err: _camera_pthread_create(&camera_t), ret = %d", ret);
		goto error_unmap_buffers;
	}

	return 0;

error_unmap_buffers:
	while (i > 0) {
		--i;
		munmap(cam->ctxt.buf[i].addr, cam->ctxt.buf[i].size);
	}
error_close_device:
	close(cam->ctxt.fd);

	return -1;
}

/**
 * @brief 反初始化 uvc 摄像头
 * @param cam 摄像头对象
 * @retval int 错误码
 */
void camera_deinit(camera_t* cam)
{
	int i = 0;

	if (NULL == cam) {
		return;
	}

	_camera_pthread_destory(cam);

	for (i = 0; i < CAMERA_BUFFER_CNT; i++) {
		munmap(cam->ctxt.buf[i].addr, cam->ctxt.buf[i].size);
		dbug(DBUG, "success: buffer index %d, munmap(%p, %d)", i, cam->ctxt.buf[i].addr, cam->ctxt.buf[i].size);
	}

	close(cam->ctxt.fd);
	dbug(DBUG, "success: close(%d)", cam->ctxt.fd);
}

/**
 * @brief 启动摄像头，开始推流
 * @param cam 摄像头对象
 */
int camera_start(camera_t* cam)
{
	int ret = 0;
	int vtype = 0;

	if (NULL == cam) {
		return -1;
	}

	vtype = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	ret = ioctl(cam->ctxt.fd, VIDIOC_STREAMON, &vtype);
	if (ret) {
		dlog(DERR, "err: ioctl(%d, VIDIOC_STREAMON, V4L2_BUF_TYPE_VIDEO_CAPTURE), ret = %d", cam->ctxt.fd, ret);
		return -1;
	}
	dbug(DBUG, "success: ioctl(%d, VIDIOC_STREAMON, V4L2_BUF_TYPE_VIDEO_CAPTURE)", cam->ctxt.fd);

	cam->ctxt.pthread_bypass = false;

	return 0;
}

/**
 * @brief 关闭摄像头，关闭推流
 * @param cam 摄像头对象
 */
void camera_stop(camera_t* cam)
{
	int ret = 0;
	int vtype = 0;

	if (NULL == cam) {
		return;
	}

	cam->ctxt.pthread_bypass = true;

	vtype = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	ret = ioctl(cam->ctxt.fd, VIDIOC_STREAMOFF, &vtype);
	if (ret) {
		dlog(DERR, "err: ioctl(%d, VIDIOC_STREAMOFF, V4L2_BUF_TYPE_VIDEO_CAPTURE), ret = %d", cam->ctxt.fd, ret);
		return;
	}
	dbug(DBUG, "success: ioctl(%d, VIDIOC_STREAMOFF, V4L2_BUF_TYPE_VIDEO_CAPTURE)", cam->ctxt.fd);
}

/**
 * @brief 注册帧处理回调接口
 * @param cam 摄像头对象
 * @param cb 帧处理回调函数
 * @retval int 错误码
 */
int camera_callback_register(camera_t* cam, camera_frame_cb_t cb)
{
	if (NULL == cam) {
		dlog(DERR, "inval: cam");
		return -1;
	}

	if (NULL == cb) {
		dlog(DERR, "inval: cb");
		return -1;
	}

	cam->ctxt.cb = cb;
	dbug(DBUG, "success: frame callback register");

	return 0;
}
