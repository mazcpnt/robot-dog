#ifndef _DISPLAY_H_
#define _DISPLAY_H_

#include <SDL2/SDL.h>
#include <stdint.h>

typedef struct _display {
	struct {
		struct {
			int width;
			int height;

			// SDL_PIXELFORMAT_ARGB8888
			// SDL_PIXELFORMAT_RGB565
			// SDL_PIXELFORMAT_YUYV
			int fmt;
		} fix;
		struct {
			int dir;
		} var;
	} cfg;
	struct {
		void* window;
		void* renderer;
		void* texture;
	} ctxt;
} display_t;

int display_init(display_t* lcd);
int display_fill(display_t* lcd, int x1, int y1, int x2, int y2, uint16_t* color);
void display_deinit(display_t* lcd);

#endif /* _DISPLAY_H_ */
