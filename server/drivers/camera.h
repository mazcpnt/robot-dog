#ifndef _DRIVERS_CAMERA_H_
#define _DRIVERS_CAMERA_H_

#include <stdbool.h>
#include <stdint.h>

#define CAMERA_BUFFER_CNT 8

enum rotate_t {
	ROTATE_0,
	ROTATE_90,
	ROTATE_180,
	ROTATE_270,
};

enum flip_t {
	FLIP_NOR, // normal
	FLIP_HOR, // horizontal
	FLIP_VER, // vertical
	FLIP_HAV, // horizontal and vertical
};

typedef struct frame {
	int index;     /* buffer index */
	int num;       /* 帧序号 */
	int width;     /* 宽度: 一行有多少个象素 */
	int height;    /* 高度: 一列有多少个象素 */
	int bpp;       /* bit per pixel: 一个象素用多少位来表示 */
	int pitch;     /* 一行数据有多少字节 */
	int size;      /* 所有字节数 */
	uint8_t* addr; /* 像素数据存储的地方 */
	uint64_t pts;  /* 时间戳 */
} frame_t;

typedef void (*camera_frame_cb_t)(int chn, frame_t* frm);

typedef struct camera {
	/* config */
	struct {
		struct {
			int chn;          /* 摄像头通道号 */
			int width;        // 摄像头宽
			int height;       // 摄像头高
			int fps;          // 摄像头帧率(暂不支持)
			char devname[32]; // 设备节点名
		} fix;
		struct {
			enum rotate_t orien; // 旋转(暂不支持)
			enum flip_t flip;    // 翻转(暂不支持)
		} var;
	} cfg;

	/* context */
	struct {
		int fd;               /* 文件句柄 */
		pthread_t pid;        /* 线程句柄 */
		bool pthread_exit;    /* 线程退出标记 */
		bool pthread_bypass;  /* 线程bypass标记 */
		camera_frame_cb_t cb; /* 帧处理回调 */
		struct {
			int width;     /* 宽度: 一行有多少个象素 */
			int height;    /* 高度: 一列有多少个象素 */
			int bpp;       /* bit per pixel: 一个象素用多少位来表示 */
			int pitch;     /* 一行数据有多少字节 */
			int size;      /* 所有字节数 */
			uint8_t* addr; /* 像素数据存储的地方 */
		} buf[CAMERA_BUFFER_CNT];

	} ctxt;
} camera_t;

int camera_init(camera_t* cam);
void camera_deinit(camera_t* cam);
int camera_start(camera_t* cam);
void camera_stop(camera_t* cam);
int camera_callback_register(camera_t* cam, camera_frame_cb_t cb);

#endif /* _DRIVERS_CAMERA_H_ */
