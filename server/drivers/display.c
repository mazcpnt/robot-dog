#include "display.h"
#include "dlog.h"
#include <stdio.h>

static int dlvl = DINF;

int display_init(display_t* lcd)
{
	int ret = 0;

	if (NULL == lcd) {
		return -1;
	}

	if (!lcd->cfg.fix.width || !lcd->cfg.fix.height) {
		return -1;
	}

	ret = SDL_Init(SDL_INIT_VIDEO);
	if (ret) {
		return -1;
	}

	lcd->ctxt.window = SDL_CreateWindow("avs",
	    SDL_WINDOWPOS_UNDEFINED,
	    SDL_WINDOWPOS_UNDEFINED,
	    lcd->cfg.fix.width,
	    lcd->cfg.fix.height,
	    SDL_WINDOW_SHOWN);
	if (NULL == lcd->ctxt.window) {
		dlog(DERR, "err: SDL_CreateWindow()");
		SDL_Quit();
		return -1;
	}

	lcd->ctxt.renderer = SDL_CreateRenderer(lcd->ctxt.window,
	    -1,
	    SDL_RENDERER_ACCELERATED);
	if (NULL == lcd->ctxt.renderer) {
		dlog(DERR, "err: SDL_CreateRenderer()");
		SDL_DestroyWindow(lcd->ctxt.window);
		SDL_Quit();
		return -1;
	}

	lcd->ctxt.texture = SDL_CreateTexture(lcd->ctxt.renderer,
	    lcd->cfg.fix.fmt,
	    SDL_TEXTUREACCESS_STREAMING,
	    lcd->cfg.fix.width,
	    lcd->cfg.fix.height);
	if (NULL == lcd->ctxt.texture) {
		dlog(DERR, "err: SDL_CreateTexture()");
		SDL_DestroyRenderer(lcd->ctxt.renderer);
		SDL_DestroyWindow(lcd->ctxt.window);
		SDL_Quit();
		return -1;
	}

	/* fixed the first render black screen */
	SDL_RenderPresent(lcd->ctxt.renderer);

	return 0;
}

int display_fill(display_t* lcd, int x1, int y1, int x2, int y2, uint16_t* color)
{
	int ret = 0;
	int picth = 0;
	SDL_Rect rect;

	if (NULL == lcd) {
		return -1;
	}

	if (NULL == color) {
		return -1;
	}

	if (NULL == lcd->ctxt.renderer) {
		return -1;
	}

	if (NULL == lcd->ctxt.texture) {
		return -1;
	}

	if (x1 < 0 || x1 >= lcd->cfg.fix.width) {
		return -1;
	}

	if (y1 < 0 || y1 >= lcd->cfg.fix.height) {
		return -1;
	}

	if (x2 < 0 || x2 >= lcd->cfg.fix.width) {
		return -1;
	}

	if (y2 < 0 || y2 >= lcd->cfg.fix.height) {
		return -1;
	}

	rect.x = x1;
	rect.y = y1;
	rect.w = x2 - x1 + 1;
	rect.h = y2 - y1 + 1;

	/* rgb5656 formmat, pre pixel is 2 byte*/
	picth = rect.w * 2;
	ret = SDL_UpdateTexture(lcd->ctxt.texture, &rect, color, picth);
	if (ret) {
		dlog(DERR, "err: SDL_UpdateTexture(), ret = %d", ret);
		return -1;
	}

	SDL_RenderClear(lcd->ctxt.renderer);

	ret = SDL_RenderCopy(lcd->ctxt.renderer, lcd->ctxt.texture, NULL, &rect);
	if (ret) {
		dlog(DERR, "err: SDL_RenderCopy(), ret = %d", ret);
		return -1;
	}

	SDL_RenderPresent(lcd->ctxt.renderer);

	return 0;
}

void display_deinit(display_t* lcd)
{
	if (NULL == lcd) {
		return;
	}

	if (lcd->ctxt.texture != NULL) {
		SDL_DestroyTexture(lcd->ctxt.texture);
	}

	if (lcd->ctxt.renderer != NULL) {
		SDL_DestroyTexture(lcd->ctxt.renderer);
	}

	if (lcd->ctxt.window != NULL) {
		SDL_DestroyTexture(lcd->ctxt.window);
	}

	SDL_Quit();
}
