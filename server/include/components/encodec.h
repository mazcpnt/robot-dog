#ifndef _COMPONENTS_ENCODEC_H_
#define _COMPONENTS_ENCODEC_H_

#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
#include <libavutil/opt.h>
#include <libswscale/swscale.h>

#include <pthread.h>
#include <stdbool.h>
#include <stdint.h>

typedef struct encodec {
	/* config */
	struct {
		struct {
			int chn; /* 摄像头通道号 */
		} fix;
		struct {
			int width;  /* 摄像头宽 */
			int height; /* 摄像头高 */
		} var;
	} cfg;

	/* context */
	struct {
		pthread_t pid;         /* 线程句柄 */
		bool pthread_exit;     /* 线程退出标记 */
		bool pthread_bypass;   /* 线程bypass标记 */
		pthread_mutex_t mutex; /* 互斥锁 */
		AVCodecContext* codec_ctx;
	} ctx;
} encodec_t;

int encodec_init(encodec_t* enc);
void encodec_deinit(encodec_t* enc);
int encodec_start(encodec_t* enc);
void encodec_stop(encodec_t* enc);
int encodec_encode(encodec_t* enc, AVFrame* frame);

#endif /* _COMPONENTS_ENCODEC_H_ */
