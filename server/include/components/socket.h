#ifndef _COMPONENTS_SOCKET_H_
#define _COMPONENTS_SOCKET_H_

#include <pthread.h>
#include <stdbool.h>
#include <stdint.h>

typedef struct socket_server {
	/* config */
	struct {
		struct {
			int res;
		} fix;
		struct {
			int res;
		} var;
	} cfg;

	/* context */
	struct {
		pthread_t pid;       /* 线程句柄 */
		bool pthread_exit;   /* 线程退出标记 */
		bool pthread_bypass; /* 线程bypass标记 */
		int server_fd;
		int client_fd;
		int bypass;
	} ctx;
} socket_server_t;

int socket_server_init(socket_server_t* skt);
void socket_server_deinit(socket_server_t* skt);
int socket_server_start(socket_server_t* skt);
void socket_server_stop(socket_server_t* skt);
int socket_server_send(socket_server_t* skt, uint8_t* buffer, uint32_t size);
int socket_server_packet_send(socket_server_t* skt, uint8_t* addr, uint32_t len, uint64_t pts);
int socket_server_recv(socket_server_t* skt, uint8_t* buffer, uint32_t size);

#endif /* _COMPONENTS_SOCKET_H_ */
