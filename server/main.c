#include <fcntl.h>
#include <linux/videodev2.h>
#include <poll.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <time.h>
#include <unistd.h>

#include "components/encodec.h"
#include "components/socket.h"
#include "convert.h"
#include "dlog.h"
#include "drivers/camera.h"
#include "drivers/display.h"

#define FRAME_INFO 0
#define FRAME_DUMP 0
#define FRAME_SHOW 0

static int dlvl = DBUG;
static int exit_flag = 0;
static encodec_t enc;
static display_t disp;
static int av_frame_init = 0;
static AVPacket* inpkt;
static AVFrame* infrm;
static AVFrame* outfrm;
static const AVCodec* pCodec;
AVCodecContext* pCodecCtx;

static int scnt = 0;
static int rcnt = 0;

socket_server_t skt = { 0 };

/**
 * @brief 信号处理函数
 */
static void signal_handler(int signo)
{
	dbug(DINF, "\n");
	exit_flag = 1;
}

static void write_image(AVFrame* frame, int chn, int num, char* suffix)
{
	int fd = 0;
	int ret = 0;
	int size = 0;
	char name[80];

	snprintf(name, sizeof(name), "capture-%d-%d.%s", chn, num, suffix);

	fd = open(name, O_WRONLY | O_CREAT);
	if (fd < 0) {
		dlog(DERR, "err: open(%s, O_WRONLY | O_CREAT), ret = %d", name, fd);
		return;
	}
	dbug(DBUG, "success: open(%s, O_WRONLY | O_CREAT), fd = %d", name, fd);

	if (AV_PIX_FMT_YUYV422 == frame->format) {
		size = frame->linesize[0] * frame->height;
		ret = write(fd, frame->data[0], size);
		if (ret != size) {
			dlog(DERR, "err: write(%d, %p, %d), ret = %d", fd, frame->data[0], size, ret);
		}
		dbug(DINF, "success: write(%d, %p, %d)", fd, frame->data[0], size);
	} else if (AV_PIX_FMT_YUV422P == frame->format) {
		size = frame->linesize[0] * frame->height;
		ret = write(fd, frame->data[0], size);
		if (ret != size) {
			dlog(DERR, "err: write(%d, %p, %d), ret = %d", fd, frame->data[0], size, ret);
		}
		dbug(DINF, "success: write(%d, %p, %d)", fd, frame->data[0], size);

		size = frame->linesize[1] * frame->height;
		ret = write(fd, frame->data[1], size);
		if (ret != size) {
			dlog(DERR, "err: write(%d, %p, %d), ret = %d", fd, frame->data[1], size, ret);
		}
		dbug(DINF, "success: write(%d, %p, %d)", fd, frame->data[1], size);

		size = frame->linesize[2] * frame->height;
		ret = write(fd, frame->data[2], size);
		if (ret != size) {
			dlog(DERR, "err: write(%d, %p, %d), ret = %d", fd, frame->data[2], size, ret);
		}
		dbug(DINF, "success: write(%d, %p, %d)", fd, frame->data[2], size);
	} else if (AV_PIX_FMT_YUV420P10LE == frame->format) {
		size = frame->linesize[0] * frame->height;
		ret = write(fd, frame->data[0], size);
		if (ret != size) {
			dlog(DERR, "err: write(%d, %p, %d), ret = %d", fd, frame->data[0], size, ret);
		}
		dbug(DINF, "success: write(%d, %p, %d)", fd, frame->data[0], size);

		size = frame->linesize[1] * frame->height / 2;
		ret = write(fd, frame->data[1], size);
		if (ret != size) {
			dlog(DERR, "err: write(%d, %p, %d), ret = %d", fd, frame->data[1], size, ret);
		}
		dbug(DINF, "success: write(%d, %p, %d)", fd, frame->data[1], size);

		size = frame->linesize[2] * frame->height / 2;
		ret = write(fd, frame->data[2], size);
		if (ret != size) {
			dlog(DERR, "err: write(%d, %p, %d), ret = %d", fd, frame->data[2], size, ret);
		}
		dbug(DINF, "success: write(%d, %p, %d)", fd, frame->data[2], size);
	}

	close(fd);
	dbug(DBUG, "success: close(%d)", fd);
}

/**
 * @brief 视频帧处理回调
 * @param frm 视频帧信息
 */
void frame_cb(int chn, frame_t* frm)
{
	int ret = 0;
	clock_t start, end;
	int timed = 0;

#if FRAME_INFO
	if (frm->num % 25 == 0) {
		dbug(DBUG, "\n%-8s %-8s %-8s %-8s %-8s %-8s %-8s %-8s %-16s %-12s",
		    "Chan",
		    "Num",
		    "Index",
		    "Width",
		    "Height",
		    "Bpp",
		    "Pitch",
		    "Size",
		    "Addr",
		    "Pts");
	}

	dbug(DBUG, "%-8d %-8d %-8d %-8d %-8d %-8d %-8d %-8d %-16p %-12ld",
	    chn,
	    frm->num,
	    frm->index,
	    frm->width,
	    frm->height,
	    frm->bpp,
	    frm->pitch,
	    frm->size,
	    frm->addr,
	    frm->pts);
#endif

	if (0 == av_frame_init) {

		inpkt = av_packet_alloc();
		if (!inpkt) {
			dlog(DERR, "err: av_packet_alloc(inpkt)");
			return;
		}

		/* 分配 MJPG 解码后图像内存 */
		infrm = av_frame_alloc();
		if (!infrm) {
			dlog(DERR, "err: av_frame_alloc(), infrm");
		}

		infrm->format = AV_PIX_FMT_YUV422P;
		infrm->color_range = AVCOL_RANGE_JPEG;
		infrm->width = frm->width;
		infrm->height = frm->height;
		ret = av_frame_get_buffer(infrm, 0);
		if (ret < 0) {
			dlog(DERR, "err: av_frame_get_buffer(), infrm");
		}

		/* 分配 YUV420P 10BIT 图像内存 */
		outfrm = av_frame_alloc();
		if (!outfrm) {
			dlog(DERR, "err: av_frame_alloc(), outfrm");
		}
		outfrm->format = AV_PIX_FMT_YUV420P10LE;
		outfrm->color_range = AVCOL_RANGE_JPEG;
		outfrm->width = frm->width;
		outfrm->height = frm->height;

		ret = av_frame_get_buffer(outfrm, 0);
		if (ret < 0) {
			dlog(DERR, "err: av_frame_get_buffer(), outfrm");
		}

		dbug(DINF, "infrm width        = %d", infrm->width);
		dbug(DINF, "infrm height       = %d", infrm->height);
		dbug(DINF, "infrm linesize[0]  = %d", infrm->linesize[0]); /* av_frame_get_buffer 之后，才会给其赋值 */
		dbug(DINF, "infrm linesize[1]  = %d", infrm->linesize[1]); /* av_frame_get_buffer 之后，才会给其赋值 */
		dbug(DINF, "infrm linesize[2]  = %d", infrm->linesize[2]); /* av_frame_get_buffer 之后，才会给其赋值 */

		dbug(DINF, "outfrm width       = %d", outfrm->width);
		dbug(DINF, "outfrm height      = %d", outfrm->height);
		dbug(DINF, "outfrm linesize[0] = %d", outfrm->linesize[0]); /* av_frame_get_buffer 之后，才会给其赋值 */
		dbug(DINF, "outfrm linesize[1] = %d", outfrm->linesize[1]); /* av_frame_get_buffer 之后，才会给其赋值 */
		dbug(DINF, "outfrm linesize[2] = %d", outfrm->linesize[2]); /* av_frame_get_buffer 之后，才会给其赋值 */

		/* 创建 MJPG 解码器 */
		pCodec = avcodec_find_decoder(AV_CODEC_ID_MJPEG);
		if (pCodec == NULL) {
			printf("Unsupported codec!\n");
			return;
		}

		pCodecCtx = avcodec_alloc_context3(pCodec);
		if (!pCodecCtx) {
			printf("Could not allocate video codec context\n");
			return;
		}

		pCodecCtx->width = 1280;
		pCodecCtx->height = 720;
		pCodecCtx->time_base = (AVRational) { 1, 30 };
		pCodecCtx->framerate = (AVRational) { 30, 1 };
		if (avcodec_open2(pCodecCtx, pCodec, NULL) < 0) {
			printf("Could not open codec\n");
			return;
		}

		av_frame_init = 1;
	}

	inpkt->data = frm->addr;
	inpkt->size = frm->size;
	avcodec_send_packet(pCodecCtx, inpkt);

	dbug(DIGN, "mjpeg send(%d)", scnt++);
	while (ret >= 0) {
		ret = avcodec_receive_frame(pCodecCtx, infrm);
		if (ret == AVERROR(EAGAIN)) {
			break;
		} else if (ret == AVERROR_EOF) {
			break;
		} else if (ret < 0) {
			dlog(DERR, "err: Error during decoding");
			return;
		}

		dbug(DIGN, "mjpeg recv(%d)", rcnt++);

		infrm->pts = frm->pts;
		outfrm->pts = infrm->pts;
		// start = clock();

		/* 像素格式转化 */
		infrm->format = AV_PIX_FMT_YUV422P;
		infrm->color_range = AVCOL_RANGE_JPEG;
		ret = convert(infrm, outfrm);
		if (ret) {
			dlog(DERR, "err: convert()");
		}

		ret = encodec_encode(&enc, outfrm);
		if (ret) {
			dlog(DERR, "err: encodec_encode()");
		}

		// end = clock();
		// timed = ((end - start) * 1000) / CLOCKS_PER_SEC;
		// dbug(DINF, "time = %d ms", timed);

#if FRAME_DUMP
		if (frm->num == 49) {
			write_image(infrm, chn, frm->num, "yuv422p");
			write_image(outfrm, chn, frm->num, "yuv420p10");
		}
#endif

#if FRAME_SHOW
		display_fill(&disp, 0, 0, disp.cfg.fix.width - 1, disp.cfg.fix.height - 1, (uint16_t*)frm->addr);
#endif

		av_frame_unref(infrm);
	}
}

int main(int argc, char* argv[])
{
	int ret = 0;
	int width = 1280;
	int height = 720;
	camera_t cam;

	signal(SIGINT, signal_handler);

#if FRAME_SHOW
	disp.cfg.fix.width = width;
	disp.cfg.fix.height = height;
	disp.cfg.fix.fmt = SDL_PIXELFORMAT_YUY2;
	display_init(&disp);
#endif

	cam.cfg.fix.width = width;
	cam.cfg.fix.height = height;
	memset(cam.cfg.fix.devname, 0, 32);
	memcpy(cam.cfg.fix.devname, "/dev/video0", strlen("/dev/video0"));

	ret = socket_server_init(&skt);
	if (ret) {
		dlog(DERR, "err: socket_server_init(&socket_server_t), ret = %d", ret);
		return -1;
	}

	ret = camera_init(&cam);
	if (ret) {
		dlog(DERR, "err: camera_init(&camera_t), ret = %d", ret);
		return -1;
	}

	ret = camera_callback_register(&cam, frame_cb);
	if (ret) {
		dlog(DERR, "err: camera_callback_register(&camera_t, frame_cb), ret = %d", ret);
		return -1;
	}

	enc.cfg.var.width = width;
	enc.cfg.var.height = height;

	ret = encodec_init(&enc);
	if (ret) {
		dlog(DERR, "err: encodec_init(&encodec_t), ret = %d", ret);
		return -1;
	}

	ret = socket_server_start(&skt);
	if (ret) {
		dlog(DERR, "err: socket_server_start(&socket_server_t), ret = %d", ret);
		return -1;
	}

	ret = camera_start(&cam);
	if (ret) {
		dlog(DERR, "err: camera_start(&camera_t), ret = %d", ret);
		return -1;
	}

	ret = encodec_start(&enc);
	if (ret) {
		dlog(DERR, "err: encodec_start(&encodec_t), ret = %d", ret);
		return -1;
	}

	while (!exit_flag) {
		sleep(5);
	}

	camera_stop(&cam);
	encodec_stop(&enc);
	socket_server_stop(&skt);
	socket_server_deinit(&skt);
	encodec_deinit(&enc);
	camera_deinit(&cam);
	av_frame_free(&infrm);
	av_frame_free(&outfrm);
	av_packet_unref(inpkt);

	return 0;
}
