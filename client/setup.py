from setuptools import setup
from distutils.core import Extension
import numpy

MOD = 'robotpy'

sources = ['main.c', 'components/decode.c', 'components/convert.c', 'components/socket.c']
include_dirs = [numpy.get_include(), '.', '../common/include', 'include']
libraries = ['avcodec', 'swscale', 'avutil', 'pthread']
library_dirs = ['../common/libraries']

setup(name = MOD,
      version = '0.10',
      description = "robot dog video capture package",
      classifiers=[ "Programming Language :: Python", ],
      author = 'aiit',
      ext_modules = [Extension(MOD, sources=sources, include_dirs=include_dirs, libraries=libraries, library_dirs=library_dirs)],
      install_requires = [ 'setuptools', ]
)
