import robotpy
import cv2
import numpy
import sys
import subprocess

# 拉流命令
# ffplay -fflags nobuffer rtmp://127.0.0.1/live/test

command = ['ffmpeg',
           '-y', '-an',
           '-f', 'rawvideo',
           '-vcodec', 'rawvideo',
           '-pix_fmt', 'bgr24',
           '-s', '1280x720',
           '-i', '-',
           '-c:v', 'libx264',
           '-pix_fmt', 'yuv420p',
           '-preset', 'ultrafast',
           '-tune', 'zerolatency',
           '-f', 'flv',
           'rtmp://127.0.0.1:1935/live/test']

pipe = subprocess.Popen(command, shell=False, stdin=subprocess.PIPE)

# 获取图像宽高帧率
width, height, fps = robotpy.info()
print("width  =", width)
print("height =", height)
print("fps    =", fps)

server_ip = "127.0.0.1"

# 启动视频采集
ret = robotpy.capture(server_ip)
if ret:
        print("err: robotpy.capture()")
        sys.exit(0)

while True:
        # 读取一帧
        ret, frame = robotpy.read()
        if ret:
                print("err: robotpy.read()")
                break

        # 显示结果帧
        # cv2.imshow('Frame', frame)

        # 按 'q' 键退出循环
        if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        pipe.stdin.write(frame.tobytes())

robotpy.release()
cv2.destroyAllWindows()
