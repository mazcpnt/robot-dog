import robotpy
import cv2
import numpy
import sys
import time

# 获取图像宽高帧率
width, height, fps = robotpy.info()
print("width  =", width)
print("height =", height)
print("fps    =", fps)

server_ip = "127.0.0.1"

# 启动视频采集
ret = robotpy.capture(server_ip)
if ret:
        print("err: robotpy.capture()")
        sys.exit(0)

start_time = time.time()

while True:
        # 读取一帧
        ret, frame = robotpy.read()
        if ret:
                print("err: robotpy.read()")
                break

        # 显示结果帧
        cv2.imshow('Frame', frame)

        # 按 'q' 键退出循环
        if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        # 检查是否已经过了20秒
        elapsed_time = time.time() - start_time
        if elapsed_time >= 10:
                print("timeout");
                break

robotpy.release()
print("robotpy.release()")
cv2.destroyAllWindows()

