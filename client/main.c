
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#ifndef PY_SSIZE_T_CLEAN
#define PY_SSIZE_T_CLEAN
#endif

#include <Python.h>
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>

#include "components/decodec.h"
#include "components/socket.h"
#include "dlog.h"

static int dlvl = DBUG;

static int width = 1280;
static int height = 720;
static int fps = 10;
static int flag = 0;
static socket_client_t skt;
static decodec_t dec;

pthread_t pid;
static uint8_t* buffer = NULL;

static PyObject* py_capture(PyObject* self, PyObject* args)
{
	int ret = 0;    /* 内部的返回值 */
	int pyret = 0;  /* 返回给 python 方法的返回值 */
	const char* ip; /* 传入的 ip 地址 */
	size_t len;     /* 传入的 ip 字符串长度 */

	if (flag) {
		dbug(DINF, "capture robotpy init");
		return Py_BuildValue("i", pyret);
	}

	// 解析传入的参数
	if (!PyArg_ParseTuple(args, "s#", &ip, &len)) {
		return NULL; // 如果解析失败，返回NULL
	}

	dbug(DINF, "success: ip is %s", ip);

	dec.cfg.var.width = width;
	dec.cfg.var.height = height;
	ret = decodec_init(&dec);
	if (ret) {
		dlog(DERR, "err: decodec_init()");
		pyret = -1;
		return Py_BuildValue("i", pyret);
	}

	memcpy(skt.cfg.var.ip, ip, len);
	skt.ctx.dec = &dec;
	ret = socket_client_init(&skt);
	if (ret < 0) {
		dlog(DERR, "err: socket_client_init(skt)");
		pyret = -1;
		return Py_BuildValue("i", pyret);
	}

	ret = decodec_start(&dec);
	if (ret) {
		dlog(DERR, "err: decodec_start()");
		pyret = -1;
		return Py_BuildValue("i", pyret);
	}

	ret = socket_client_start(&skt);
	if (ret) {
		dlog(DERR, "err: socket_client_start()");
		pyret = -1;
		return Py_BuildValue("i", pyret);
	}

	flag = 1;
	return Py_BuildValue("i", pyret);
}

static PyObject* py_release(PyObject* self, PyObject* args)
{
	int pyret = 0; /* 返回给 python 方法的返回值 */

	if (0 == flag) {
		dbug(DINF, "robotpy not init");
		return Py_BuildValue("i", pyret);
	}

	socket_client_stop(&skt);
	decodec_stop(&dec);
	sleep(2);
	socket_client_deinit(&skt);
	decodec_deinit(&dec);

	flag = 0;
	return Py_BuildValue("i", pyret);
}

static PyObject* py_read(PyObject* self, PyObject* args)
{
	int ret = 0;
	int bpp = 3;
	int ndim = 3;
	int pyret = 0;
	uint32_t size = 0;
	npy_intp shape[3] = { height, width, bpp };
	PyObject* tuple = NULL;

	if (0 == flag) {
		dbug(DINF, "robotpy not init");
		pyret = -1;
		PyTuple_SetItem(tuple, 0, PyLong_FromLong(pyret));
		PyTuple_SetItem(tuple, 1, NULL);
		return tuple;
	}

	ret = decode_frame_read(buffer, &size);
	if (ret) {
		dlog(DERR, "err: decode_frame_read()");
		pyret = -1;
		PyTuple_SetItem(tuple, 0, PyLong_FromLong(pyret));
		PyTuple_SetItem(tuple, 1, NULL);
		return tuple;
	}

	PyObject* array = PyArray_SimpleNewFromData(ndim, shape, NPY_UINT8, buffer);
	if (array == NULL) {
		dlog(DERR, "err: PyArray_SimpleNewFromData");
		pyret = -1;
		PyTuple_SetItem(tuple, 0, PyLong_FromLong(pyret));
		PyTuple_SetItem(tuple, 1, NULL);
		return tuple;
	}

	tuple = PyTuple_New(2);
	if (tuple == NULL) {
		Py_DECREF(array);
		pyret = -1;
		PyTuple_SetItem(tuple, 0, PyLong_FromLong(pyret));
		PyTuple_SetItem(tuple, 1, NULL);
		return tuple;
	}

	PyTuple_SetItem(tuple, 0, PyLong_FromLong(pyret));
	PyTuple_SetItem(tuple, 1, array);
	return tuple;
}

static PyObject* py_info(PyObject* self, PyObject* args)
{
	PyObject* tuple = PyTuple_New(3);

	PyTuple_SetItem(tuple, 0, PyLong_FromLong(width));
	PyTuple_SetItem(tuple, 1, PyLong_FromLong(height));
	PyTuple_SetItem(tuple, 2, PyLong_FromLong(fps));
	return tuple;
}

/* Define the methods in module */
static PyMethodDef robotpymethods[] = {
	{ "capture", py_capture, METH_VARARGS },
	{ "release", py_release, METH_VARARGS },
	{ "info", py_info, METH_VARARGS },
	{ "read", py_read, METH_VARARGS },
	{ NULL, NULL, 0, NULL }
};

/* Define the module */
static struct PyModuleDef robotpymodule = {
	PyModuleDef_HEAD_INIT,
	"robotpy",
	NULL,
	-1,
	robotpymethods
};

/* initialization function */
PyMODINIT_FUNC PyInit_robotpy(void)
{
	int bpp = 3;
	int size = width * height * bpp;

	buffer = (uint8_t*)malloc(size);
	if (!buffer) {
		printf("err: malloc\n");
		return NULL;
	}

	import_array();

	return PyModule_Create(&robotpymodule);
}
