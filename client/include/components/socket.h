#ifndef _COMPONENTS_SOCKET_CLIENT_H_
#define _COMPONENTS_SOCKET_CLIENT_H_

#include <libavcodec/avcodec.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdint.h>

typedef struct socket_client {
	/* config */
	struct {
		struct {
			int res;
		} fix;
		struct {
			char ip[32];
		} var;
	} cfg;

	/* context */
	struct {
		pthread_t pid;       /* 线程句柄 */
		bool pthread_exit;   /* 线程退出标记 */
		bool pthread_bypass; /* 线程bypass标记 */
		bool disconnect;     /* socket 断开连接标记 */
		int socket_fd;
		uint8_t* ptr;
		decodec_t* dec;
	} ctx;
} socket_client_t;

int socket_client_init(socket_client_t* skt);
void socket_client_deinit(socket_client_t* skt);
int socket_client_start(socket_client_t* skt);
void socket_client_stop(socket_client_t* skt);
int socket_client_send(socket_client_t* skt, uint8_t* buffer, uint32_t size);
int socket_client_packet_recv(socket_client_t* skt, AVPacket* packet);
int socket_client_recv(socket_client_t* skt, uint8_t* buffer, uint32_t size);

#endif /* _COMPONENTS_SOCKET_H_ */
