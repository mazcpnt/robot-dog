#ifndef _COMPONENTS_DECODEC_H_
#define _COMPONENTS_DECODEC_H_

#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
#include <libavutil/opt.h>
#include <libswscale/swscale.h>

#include <pthread.h>
#include <stdbool.h>
#include <stdint.h>

typedef struct decodec {
	/* config */
	struct {
		struct {
			int chn; /* 摄像头通道号 */
		} fix;
		struct {
			int width;  /* 摄像头宽 */
			int height; /* 摄像头高 */
		} var;
	} cfg;

	/* context */
	struct {
		pthread_t pid;         /* 线程句柄 */
		bool pthread_exit;     /* 线程退出标记 */
		bool pthread_bypass;   /* 线程bypass标记 */
		pthread_mutex_t mutex; /* 互斥锁 */
		AVCodecContext* codec_ctx;
	} ctx;
} decodec_t;

int decodec_init(decodec_t* enc);
void decodec_deinit(decodec_t* enc);
int decodec_start(decodec_t* enc);
void decodec_stop(decodec_t* enc);
int decodec_decode(decodec_t* enc, AVPacket* packet);
int decode_frame_read(uint8_t* data, uint32_t* size);

#endif /* _COMPONENTS_DECODEC_H_ */
