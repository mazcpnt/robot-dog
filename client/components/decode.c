#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>

#include "components/decodec.h"

#include "convert.h"
#include "dlog.h"

#define DECODE_FRAME_DUMP 0

#define BUF_CNT            16
#define BUF_IDLE_LEN(r, w) ((w >= r) ? (BUF_CNT + r - w - 1) : (r - w - 1))
#define BUF_FILL_LEN(r, w) ((w >= r) ? (w - r) : (BUF_CNT + w - r))
#define BUF_IDX_UPDATE(rw) (((rw + 1) >= BUF_CNT) ? 0 : (rw + 1))

static AVFrame* buffer[BUF_CNT];
static int idxr = 0;
static int idxw = 0;

static int dlvl = DBUG;

bool _buffer_is_full()
{
	return (BUF_IDLE_LEN(idxr, idxw) == 0);
}

bool _buffer_is_empty()
{
	return (BUF_FILL_LEN(idxr, idxw) == 0);
}

/**
 * @brief 创建乒乓 buffer
 * @param cnt 乒乓 buffer 的个数
 * @param size 乒乓 buffer 的大小
 */
int _buffer_create(int width, int height)
{
	int i = 0;
	int ret = 0;

	if (width <= 0) {
		dlog(DERR, "inval: width");
	}

	if (height <= 0) {
		dlog(DERR, "inval: height");
	}

	for (i = 0; i < BUF_CNT; i++) {
		buffer[i] = av_frame_alloc();
		if (!buffer[i]) {
			dlog(DERR, "err: av_frame_alloc(%d)", i);
			return -1;
		}

		buffer[i]->format = AV_PIX_FMT_BGR24;
		buffer[i]->width = width;
		buffer[i]->height = height;
		ret = av_frame_get_buffer(buffer[i], 0);
		if (ret < 0) {
			dlog(DERR, "err: av_frame_get_buffer(%d)", i);
			return -1;
		}
	}

	return 0;
}

/**
 * @brief 销毁乒乓 buffer
 */
void _buffer_destory()
{
	int i = 0;
	AVFrame* frame = NULL;

	for (i = 0; i < BUF_CNT; i++) {
		frame = buffer[i];
		av_frame_free(&frame);
	}
}

#if 1
int _buffer_write(AVFrame* frame)
{
	if (NULL == frame) {
		dlog(DERR, "inval: frame");
		return -1;
	}

	if (_buffer_is_full()) {
		// dbug(DWAN, "warning: buffer is full, %d/%d", idxr, idxw);
		return -1;
	}

	convert(frame, buffer[idxw]);
	buffer[idxw]->pts = frame->pts;
	dbug(DIGN, "avframe write index %d", idxw);
	idxw = BUF_IDX_UPDATE(idxw);

	return 0;
}
#else
int _buffer_write(AVFrame* frame)
{
	if (NULL == frame) {
		dlog(DERR, "inval: frame");
		return -1;
	}

	if (_buffer_is_full()) {
		dbug(DWAN, "warning: buffer is full, %d/%d", idxr, idxw);
		idxr = BUF_IDX_UPDATE(idxr);
	}

	convert(frame, buffer[idxw]);
	buffer[idxw]->pts = frame->pts;
	dbug(DBUG, "avframe write index %d", idxw);
	idxw = BUF_IDX_UPDATE(idxw);

	return 0;
}
#endif

#if DECODE_FRAME_DUMP
static void yuv420p10le_save(AVFrame* frame, FILE* outfile)
{
	int i;

	for (i = 0; i < frame->height; i++) {
		fwrite(frame->data[0] + i * frame->linesize[0], 2, frame->width, outfile);
	}
	for (i = 0; i < frame->height / 2; i++) {
		fwrite(frame->data[1] + i * frame->linesize[1], 2, frame->width / 2, outfile);
	}
	for (i = 0; i < frame->height / 2; i++) {
		fwrite(frame->data[2] + i * frame->linesize[2], 2, frame->width / 2, outfile);
	}

	dbug(DINF, "yuv420p10le_save()");
}

static void rgb24_save(AVFrame* frame, FILE* outfile)
{
	fwrite(frame->data[0], 1, frame->linesize[0] * frame->height, outfile);
	dbug(DINF, "rgb24_save()");
}
#endif

/**
 * @brief 解码线程
 * @param arg 解码器对象
 */
static void* _decodec_pthread(void* arg)
{
	int ret = 0;
	int rcnt = 0;
#if DECODE_FRAME_DUMP
	FILE* file = NULL;
	const char* filename = "out.rgb24";
#endif
	AVFrame* frame = NULL;
	AVCodecContext* dec_ctx = NULL;
	decodec_t* dec = (decodec_t*)arg;
	dec_ctx = dec->ctx.codec_ctx;

	/* Frame 分配内存 */
	frame = av_frame_alloc();
	if (!frame) {
		dlog(DERR, "err: av_frame_alloc(frame)");
		return NULL;
	}

	ret = _buffer_create(dec->cfg.var.width, dec->cfg.var.height);
	if (ret) {
		dlog(DERR, "err: _buffer_create()");
		return NULL;
	}

#if DECODE_FRAME_DUMP
	file = fopen(filename, "wb");
	if (!file) {
		dlog(DERR, "err: fopen(%s)\n", filename);
		exit(1);
	}
#endif

	while (!dec->ctx.pthread_exit) {
		if (dec->ctx.pthread_bypass) {
			usleep(5000);
			continue;
		}

		pthread_mutex_lock(&dec->ctx.mutex);
		ret = avcodec_receive_frame(dec_ctx, frame);
		pthread_mutex_unlock(&dec->ctx.mutex);
		if (ret == AVERROR(EAGAIN)) {
			usleep(2000);
			continue;
		} else if (ret == AVERROR_EOF) {
			usleep(2000);
			continue;
		} else if (ret < 0) {
			dlog(DERR, "err: Error during decoding");
			return NULL;
		}

		dbug(DIGN, "avcodec_receive_frame(%d), pts = %ld", rcnt++, frame->pts);
		_buffer_write(frame);

#if DECODE_FRAME_DUMP
		if (rcnt == 10) {
			// yuv420p10le_save(frame, file);
			rgb24_save(rgb_frame, file);
			fclose(file);
		}
#endif
	}

	av_frame_free(&frame);
	_buffer_destory();
	return NULL;
}

static int _decodec_pthread_create(decodec_t* dec)
{
	int ret = 0;

	dec->ctx.pthread_bypass = true;
	dec->ctx.pthread_exit = false;
	ret = pthread_create(&dec->ctx.pid, NULL, _decodec_pthread, dec);
	if (ret) {
		dlog(DERR, "err: pthread_create(pthread, NULL, _decodec_pthread, &decodec_t), ret = %d", ret);
		return -1;
	}
	dbug(DBUG, "success: pthread_create(pthread, NULL, _decodec_pthread, &decodec_t)");

	return 0;
}

static int _decodec_pthread_destory(decodec_t* dec)
{
	int ret = 0;

	dec->ctx.pthread_exit = true;
	ret = pthread_join(dec->ctx.pid, NULL);
	if (ret) {
		dlog(DERR, "err: pthread_join(dec_pthread, NULL), ret = %d", ret);
		return -1;
	}
	dbug(DINF, "success: pthread_join(dec_pthread, NULL)");

	return 0;
}

int decodec_init(decodec_t* dec)
{
	int ret = 0;
	const AVCodec* codec = NULL;
	AVCodecContext* dec_ctx = NULL;

	if (NULL == dec) {
		dlog(DERR, "inval: dec");
	}

	codec = avcodec_find_decoder(AV_CODEC_ID_H266);
	if (!codec) {
		dlog(DERR, "err: avcodec_find_decoder(AV_CODEC_ID_H266)");
		return -1;
	}

	/* 获取编码器上下文，编码参数需要在上下文中进行配置 */
	dec_ctx = avcodec_alloc_context3(codec);
	if (!dec_ctx) {
		dlog(DERR, "avcodec_alloc_context3()\n");
		return -1;
	}

	/* 配置编码参数 */
	dec_ctx->width = dec->cfg.var.width;
	dec_ctx->height = dec->cfg.var.height;
	dec_ctx->time_base = (AVRational) { 1, 10 };
	dec_ctx->framerate = (AVRational) { 10, 1 };
	dec_ctx->thread_count = 16;
	dec_ctx->pix_fmt = AV_PIX_FMT_YUV420P10LE;
	av_opt_set(dec_ctx->priv_data, "preset", "faster", 0);

	/* 打开编码器 */
	ret = avcodec_open2(dec_ctx, codec, NULL);
	if (ret < 0) {
		dlog(DERR, "avcodec_open2(%s)\n", av_err2str(ret));
		return -1;
	}

	/* 将编码器上下文保存在 dec 对象中 */
	dec->ctx.codec_ctx = dec_ctx;

	/* 创建互斥锁 */
	ret = pthread_mutex_init(&dec->ctx.mutex, 0);
	if (ret) {
		dlog(DERR, "err: pthread_mutex_init()");
	}

	/* 创建解码线程 */
	_decodec_pthread_create(dec);

	return 0;
}

void decodec_deinit(decodec_t* dec)
{
	if (NULL == dec) {
		dlog(DERR, "inval: dec");
		return;
	}

	if (NULL == dec->ctx.codec_ctx) {
		dlog(DERR, "inval: dec_ctx");
		return;
	}

	_decodec_pthread_destory(dec);
	pthread_mutex_destroy(&dec->ctx.mutex);
	avcodec_free_context(&dec->ctx.codec_ctx);
}

int decodec_start(decodec_t* dec)
{
	if (NULL == dec) {
		dlog(DERR, "inval: dec");
		return -1;
	}

	dec->ctx.pthread_bypass = false;
	return 0;
}

void decodec_stop(decodec_t* dec)
{
	if (NULL == dec) {
		dlog(DERR, "inval: dec");
		return;
	}

	dec->ctx.pthread_bypass = true;
}

int decodec_decode(decodec_t* dec, AVPacket* packet)
{
	int ret = 0;
	static int dcnt = 0;

	if (NULL == dec) {
		dlog(DERR, "inval: dec");
		return -1;
	}

	if (NULL == dec->ctx.codec_ctx) {
		dlog(DERR, "inval: dec_ctx");
		return -1;
	}

	pthread_mutex_lock(&dec->ctx.mutex);
	ret = avcodec_send_packet(dec->ctx.codec_ctx, packet);
	pthread_mutex_unlock(&dec->ctx.mutex);

	if (ret < 0) {
		dlog(DERR, "err: avcodec_send_packet(), ret = %d", ret);
		return -1;
	}
	dbug(DIGN, "avcodec_send_packet(%d), pts = %ld", dcnt++, packet->pts);

	return 0;
}

int decode_frame_read(uint8_t* data, uint32_t* size)
{
	int len = 0;

	if (NULL == data) {
		dlog(DERR, "inval: data");
		return -1;
	}

	if (NULL == size) {
		dlog(DERR, "inval: size");
		return -1;
	}

	while (_buffer_is_empty()) {
		usleep(5000);
	}

	len = buffer[idxr]->linesize[0] * buffer[idxr]->height;
	memcpy(data, buffer[idxr]->data[0], len);
	idxr = BUF_IDX_UPDATE(idxr);
	*size = len;

	return 0;
}
