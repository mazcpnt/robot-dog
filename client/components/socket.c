#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include "components/decodec.h"
#include "components/socket.h"
#include "dlog.h"
#include "header.h"

static int dlvl = DINF;

#define PORT            8080
#define SLICE_LEN       1400
#define STREAM_SIZE_MAX (512 * 1024)

void _socket_client_disconnect(socket_client_t* skt)
{
	int fd = 0;               /* socket 通信的 fd 文件句柄 */
	int scnt = 0;             /* send 返回给 server 的数据长度 */
	int rcnt = 0;             /* recv 接收到 server 的数据长度 */
	int size = 0;             /* recv 用于接收 server 发送过来的数据长度 */
	int head_len = 0;         /* 消息头长度 */
	char buf[1600] = { 0 };   /* 接收消息缓冲区 */
	aiit_head_t head = { 0 }; /* 码流消息头 */

	if (NULL == skt) {
		dlog(DERR, "inval: skt");
		return;
	}

	fd = skt->ctx.socket_fd;
	head_len = sizeof(aiit_head_t);
	size = SLICE_LEN + head_len;

	rcnt = recv(fd, buf, size, 0);
	if (rcnt < head_len) {
		return;
	} else {
		memcpy(&head, buf, head_len);
	}

	if (rcnt != (int)head.slice_len) {
		dlog(DERR, "err: socket recv %d/%d\n", rcnt, head.slice_len);
	}

	/* 返回 EXIT */
	head.sync_code[0] = 'E';
	head.sync_code[1] = 'X';
	head.sync_code[2] = 'I';
	head.sync_code[3] = 'T';
	scnt = send(fd, &head, head_len, 0);
	if (scnt != head_len) {
		dlog(DERR, "err: socket send %d/%d\n", scnt, head_len);
	}

	dbug(DINF, "success: client disconnect");
}

static void* _socket_client_pthread(void* arg)
{
	AVPacket* packet;
	socket_client_t* skt = (socket_client_t*)arg;

	packet = av_packet_alloc();
	if (!packet) {
		dlog(DERR, "err: av_packet_alloc(packet)");
		return NULL;
	}

	packet->data = av_malloc(STREAM_SIZE_MAX);
	if (NULL == packet->data) {
		dlog(DERR, "err: av_malloc(%d)\n", STREAM_SIZE_MAX);
		return NULL;
	}

	while (!skt->ctx.pthread_exit) {
		if (skt->ctx.pthread_bypass) {
			if (skt->ctx.disconnect) {
				_socket_client_disconnect(skt);
				break;
			} else {
				usleep(5000);
				continue;
			}
		}

		socket_client_packet_recv(skt, packet);
		decodec_decode(skt->ctx.dec, packet);
	}

	av_packet_free(&packet);
	return NULL;
}

static int _socket_client_pthread_create(socket_client_t* skt)
{
	int ret = 0;

	skt->ctx.pthread_bypass = true;
	skt->ctx.pthread_exit = false;
	ret = pthread_create(&skt->ctx.pid, NULL, _socket_client_pthread, skt);
	if (ret) {
		dlog(DERR, "err: pthread_create(pthread, NULL, _socket_client_pthread, &socket_client_t), ret = %d", ret);
		return -1;
	}
	dbug(DBUG, "success: pthread_create(pthread, NULL, _socket_client_pthread, &socket_client_t)");

	return 0;
}

static int _socket_client_pthread_destory(socket_client_t* skt)
{
	int ret = 0;

	skt->ctx.pthread_exit = true;
	ret = pthread_join(skt->ctx.pid, NULL);
	if (ret) {
		dlog(DERR, "err: pthread_join(skt_pthread, NULL), ret = %d", ret);
		return -1;
	}
	dbug(DINF, "success: pthread_join(skt_pthread, NULL)");

	return 0;
}

int socket_client_init(socket_client_t* skt)
{
	int fd = 0;
	int ret = 0;
	struct sockaddr_in serv_addr;

	// 创建 socket 文件描述符
	fd = socket(AF_INET, SOCK_STREAM, 0);
	if (fd < 0) {
		dlog(DERR, "err: socket(AF_INET, SOCK_STREAM, 0)");
		return -1;
	}

	// 将 IPv4 地址从文本转换为二进制形式
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);
	ret = inet_pton(AF_INET, skt->cfg.var.ip, &serv_addr.sin_addr);
	if (ret <= 0) {
		dlog(DERR, "err: inet_pton(AF_INET, %s, serv_addr)", skt->cfg.var.ip);
		return -1;
	}

	// 连接到服务器
	ret = connect(fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
	if (ret < 0) {
		dlog(DERR, "err: connect(%d, serv_addr, sizeof(serv_addr))", fd);
		return -1;
	}

	// 启动取流线程
	_socket_client_pthread_create(skt);

	skt->ctx.socket_fd = fd;
	return 0;
}

void socket_client_deinit(socket_client_t* skt)
{
	if (NULL == skt) {
		dlog(DERR, "inval: skt");
		return;
	}

	_socket_client_pthread_destory(skt);
	close(skt->ctx.socket_fd);
}

int socket_client_start(socket_client_t* skt)
{
	if (NULL == skt) {
		dlog(DERR, "inval: skt");
		return -1;
	}

	skt->ctx.pthread_bypass = false;
	skt->ctx.disconnect = false;
	return 0;
}

void socket_client_stop(socket_client_t* skt)
{
	if (NULL == skt) {
		dlog(DERR, "inval: skt");
		return;
	}

	skt->ctx.disconnect = true;
	skt->ctx.pthread_bypass = true;
}

int socket_client_send(socket_client_t* skt, uint8_t* buffer, uint32_t size)
{
	if (NULL == skt) {
		dlog(DERR, "inval: skt");
		return -1;
	}

	return send(skt->ctx.socket_fd, buffer, size, 0);
}

int socket_client_packet_recv(socket_client_t* skt, AVPacket* packet)
{
	int i = 0;
	int fd = 0;               /* socket 通信的 fd 文件句柄 */
	int scnt = 0;             /* send 返回给 server 的数据长度 */
	int rcnt = 0;             /* recv 接收到 server 的数据长度 */
	int size = 0;             /* recv 用于接收 server 发送过来的数据长度 */
	int num = 0;              /* 接收帧计数 */
	int head_len = 0;         /* 消息头长度 */
	char buf[1600] = { 0 };   /* 接收消息缓冲区 */
	aiit_head_t head = { 0 }; /* 码流消息头 */

	if (NULL == skt) {
		dlog(DERR, "inval: skt");
		return -1;
	}

	fd = skt->ctx.socket_fd;
	head_len = sizeof(aiit_head_t);
	size = SLICE_LEN + head_len;

	/**
	 * 如果有多个分片，则循环接收完所有的分片
	 * 先认为只有一个分片， head.slice_cnt 的值将会在读取到第一帧后进行更新
	 */
	head.slice_cnt = 1;
	for (i = 0; i < head.slice_cnt; i++) {
		rcnt = recv(fd, buf, size, 0);
		if (rcnt < head_len) {
			return -1;
		} else {
			memcpy(&head, buf, head_len);
		}

		if (rcnt != (int)head.slice_len) {
			dlog(DERR, "err: socket recv %d/%d\n", rcnt, head.slice_len);
		}

		if (i != head.slice_num) {
			dlog(DERR, "err: head.slice_num %d/%d\n", i, head.slice_num);
		}

		/* 原封不动的返回 AIIT 头信息, 表示成功接收 */
		scnt = send(fd, &head, head_len, 0);
		if (scnt != head_len) {
			dlog(DERR, "err: socket send %d/%d\n", scnt, head_len);
		}

		/* 拷贝分片数据 */
		memcpy(packet->data + head.slice_num * SLICE_LEN, buf + head_len, head.slice_len - head_len);
		dbug(DIGN, "%d/%d %d", head.slice_num, head.slice_cnt, rcnt);

		if ((head.slice_cnt - 1) == head.slice_num) {
			dbug(DIGN, "frame %d size = %d", num++, head.frame_len);
			dbug(DIGN, "==============================");
			packet->size = head.frame_len;
			packet->pts = head.frame_pts;
		}
	}

	return 0;
}

/**
 * @brief
 * @param skt
 * @param buffer 接收缓存区
 * @param size 接收缓存区大小
 * @return 实际接收的数据大小
 */
int socket_client_recv(socket_client_t* skt, uint8_t* buffer, uint32_t size)
{
	if (NULL == skt) {
		dlog(DERR, "inval: skt");
		return -1;
	}

	return recv(skt->ctx.socket_fd, buffer, size, 0);
}
